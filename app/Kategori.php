<?php

namespace App;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kategori extends Model
{
    use Cachable;
	use SoftDeletes;
    protected $table = 'kategori';
 	protected $primaryKey = 'id_kategori';
    protected $fillable = [
        'nama_kategori',
    ];
    protected $dates = ['deleted_at'];

    public function buku()
    {
        return $this->hasMany('App\Buku','id_kategori');
    }
}
