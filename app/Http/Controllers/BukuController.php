<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cache;
use App\Buku;
use App\Kategori;
use Validator;
use DB;

class BukuController extends Controller
{
    public function index()
    {
        
        $kategori = Kategori::all();
        $buku = Buku::paginate(5);
        Cache::rememberForever('buku', function() {
            return DB::table('buku')->get();
        });
        return view('buku.buku',compact('buku','kategori'));
    }
    public function deleted()
    {
        $sampah = DB::table('buku','kategori')->whereNotNull('deleted_at')->get();
        return view('buku.trash',compact('sampah'));
    }
    public function store(Request $request)
    {
        $buku['judul'] = $request->judul;;
        $buku['id_kategori'] = (int)$request->kategori;
        $buku['penulis'] = $request->penulis;
        $buku['tahun'] = $request->tahun;
        $buku['penerbit'] = $request->penerbit;

        $validasi = Validator::make($buku, [
            'judul'     => 'required|max:100',
            'id_kategori'    => 'required|max:100',
            'penulis' => 'required|max:100',
            'tahun'    => 'required|max:4',
            'penerbit' => 'required|max:100',
        ]);

        if ($validasi->fails()) {
            return redirect('buku')
                    ->withInput()
                    ->withErrors($validasi);
        }

        Buku::create($buku);
        return view('buku.buku',compact('buku'));
    }
    public function update(Request $request, $id)
    {    
    	$buku = Buku::findOrFail($id);

        $validasi = Validator::make($buku, [
            'judul'     => 'required|max:100',
            'id_kategori'    => 'required|max:100',
            'penulis' => 'required|max:100',
            'tahun'    => 'required|max:4',
            'penerbit' => 'required|max:100',
        ]);

        if ($validasi->fails()) {
            return redirect('buku')
                    ->withInput()
                    ->withErrors($validasi);
        }

    	$buku->update($request->all());
        Cache::forget('buku');
        return view('buku.buku',compact('buku'));
    }
    public function delete($id)
    {
        $buku = Buku::where('id',$id);
        $buku->delete();
        return redirect('buku');
    }
    public function userbuku()
    {
        
        $kategori = Kategori::all();
        $buku = Buku::paginate(5);
        return view('buku.userbuku',compact('buku','kategori'));
    }
}
