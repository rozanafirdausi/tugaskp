<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use DB;

class KelolaController extends Controller
{
    public function index()
    {
    	$admin = User::paginate(5);
    	return view('kelola.admin',compact('admin'));
    }
    public function deleted(Request $request)
    {
    	$trash = DB::table('admins')->whereNotNull('deleted_at')->get();
    	return view('kelola.trash',compact('trash'));
    }
    public function store(Request $request)
    {
        User::create($request->all());
        return redirect('kelola');
    }
    public function update(Request $request, $id)
    {
    	$admin = User::findOrFail($id);
        $admin['name'] = $request->name;
        $admin['email'] = (int)$request->email;
        $admin['password'] = Hash::make($request->password);
    	$admin->update($request->all());
    	return redirect('kelola');
    }
    public function delete($id)
    {
    	// $admin = Admin::withTrashed()->findOrFail($id_admin)->restore();
    	$admin = User::where('id',$id);
    	$admin->delete();
    	return redirect('kelola');
    }
}
