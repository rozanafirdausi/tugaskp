<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->admin == 0) 
        {
            return view ('user');
        }
        else
        {
            $users['users'] = \App\User::all();
            return view ('admin1',$users);
        }
    }
}
