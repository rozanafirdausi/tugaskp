<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function homepage()
    {
    	return view('welcome');
    }
    public function login()
    {
    	return view('login');
    }
    public function admin()
    {
    	return view('admin');
    }
    public function user()
    {
        return view('user');
    }
}
