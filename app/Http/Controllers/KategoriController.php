<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use Cache;
use DB;

class KategoriController extends Controller
{
    public function index()
    {
        
    	$kategori = Kategori::paginate(5);
    	return view('kategori.kategori',compact('kategori'));
        $value = Cache::get('kategori');
    }
    public function deleted()
    {
        $trash = DB::table('kategori')->whereNotNull('deleted_at')->get();
        return view('kategori.trash',compact('trash'));
    }
    public function store(Request $request)
    {
        Cache::put('kategori', '$kategori', 60);
    	Kategori::create($request->all());
        return redirect('kategori');
    }
    public function update(Request $request, $id_kategori)
    {
        Cache::forget('kategori');        
    	$kategori = Kategori::findOrFail($id_kategori);
    	$kategori->update($request->all());
    	return redirect('kategori');
    }
    public function delete($id_kategori)
    {
        $kategori = Kategori::where('id_kategori',$id_kategori);
        $kategori->delete();
        return redirect('kategori');
    }
    // public function destroy($id_kategori)
    // {
    // 	$kategori = Kategori::where('id_kategori',$id_kategori)->delete();
    // 	return back();
    // }
}
