<?php

namespace App;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Buku extends Model
{
    use SoftDeletes;
    use Cachable;
    protected $table = 'buku';
 	protected $primaryKey = 'id';
    protected $fillable = [
        'judul',
        'id_kategori',
        'penulis',
        'tahun',
        'penerbit',
    ];
    protected $dates = ['deleted_at'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'id_kategori');
    }
}