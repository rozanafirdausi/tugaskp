<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SI Kelola Buku</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">

  <!-- Place your favicon.ico and apple-touch-icon.png in the template root directory -->
  <link href="favicon.ico" rel="shortcut icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate-css/animate.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">
  <link href="css/table.css" rel="stylesheet">
  <link href="css/keuangan.css" rel="stylesheet">
  <!-- =======================================================
    Theme Name: Imperial
    Theme URL: https://bootstrapmade.com/imperial-free-onepage-bootstrap-theme/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>
<body>
  <header id="header">
      <div class="container-fluid" style="min-height:500px">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">SI Kelola Buku</a>
        </div>
        <ul class="nav navbar-nav">
          <li><a href="admin">Home</a></li>
          <li><a href="kategori">Kategori</a></li>
          <li><a href="buku">Buku</a></li>
          <li><a href="kelola">Kelola User</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
      </div>
  </header>
  
<body>
  <div id="preloader"></div>
  <div class="container fluid">
    <h1>Data Kelola User</h1>
    <div class="col-sm-12">
      <div class="panel panel-default panel-table" id="table">
        <div class="panel-heading">
          <div class="row">
            <div class="col col-xs-6">
            </div>
            <div class="col col-xs-6 text-right">
              <button type="button" class="btn btn-sm btn-primary btn-create" data-toggle="modal" data-target="#add-modal">Create New</button>
            </div>
          </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Tambah User</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="/kelola" method="post">
                  {{ csrf_field() }}
                  <input type=hidden name=_token value="{{ csrf_token() }}">
                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                      <label for="item">Nama User : </label>
                      <input name="name" type="text" class="form-control" id="item" placeholder="masukkan nama user" required>
                      @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                    </div>
                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                      <label for="debit">Email : </label>
                      <input name="email" type="text" class="form-control" id="debit" placeholder="masukkan email" required>
                      @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                    </div>
                    <div class="form-group @if ($errors->has('password')) has-error @endif">
                      <label for="keterangan">Password : </label>
                      <input name="password" type="password" class="form-control" id="keterangan" placeholder="masukkan password" required>
                      @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                    </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        @foreach ($admin as $data)
        <div class="modal fade" id="edit-modal-{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Edit User</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{ URL::to('/kelola/'.$data->id.'/update')}}" method="post">
                  {{ csrf_field() }}
                  <input type=hidden name=_token value="{{ csrf_token() }}">
                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                      <label for="item"><span style="color: red">*</span> Nama User : </label>
                      <input name="name" type="text" class="form-control" id="item" placeholder="masukkan nama user" value="{{$data->name}}" required>
                      @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                    </div>
                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                      <label for="item"><span style="color: red">*</span> Email : </label>
                      <input name="email" type="text" class="form-control" id="item" placeholder="masukkan email" value="{{$data->email}}" required>
                      @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                    </div>
                    <div class="form-group @if ($errors->has('password')) has-error @endif">
                      <label for="item"><span style="color: red">*</span> Password : </label>
                      <input name="password" type="password" class="form-control" id="item" placeholder="masukkan alamat" value="{{$data->password}}" required>
                      @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                    </div>                    
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        @endforeach
          <div class="panel-body">
          <table class="table table-striped table-bordered table-list">
            <thead>
              <tr>
                  <th><em class="fa fa-cog"></em></th>
                  <th>Nama User</th>
                  <th>Email User</th>
                  <th>Password</th>
                  <th>Status</th>
              </tr> 
            </thead>
            <tbody>
              @foreach ($admin as $data)
              <tr>
                <td align="center">
                  <div class="row">
                    <div class="col-sm-4">
                    <a class="btn btn-default" data-toggle="modal" data-target="#edit-modal-{{$data->id}}" href="{{URL::to('/kelola/'.$data->id)}}" method="get">
                        <em class="fa fa-pencil">
                        </em>
                    </a></div>
                    <div class="col-sm-4">
                  <form action="/kelola/{{$data->id}}" method="POST">{{ csrf_field() }}<button type="submit" class="btn btn-danger"><em class="fa fa-trash"></em></button></form></div>
                </div>
                </td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->email }}</td>
                
                <style>.hidetext { -webkit-text-security: disc; /* Default */ }</style>
                <td class="hidetext">{{ $data->password }}</td>
                <!-- <td>{{ $data->jenis_kelamin }}</td> -->
                
                @if ($data->admin === 1)
                    <td>Admin</td>
                @else
                    <td>User</td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
  </div>

<style>
.jumlah-data, .paging {
    width: 50%;
    height: 50px;
    display: block;
}

.jumlah-data {
    float: left;
    padding-top: 15px;
}

.paging {
    float: right;
    text-align: right;
}

.paging ul.pager, .paging ul.pagination {
    text-align: right;
}

ul.pagination, ul.pager {
    margin-top: 8px;
}
</style>
        <div class="panel-footer">
          <div class="row">
            <div class="col col-xs-12">
                <div class="paging">
                    {{ $admin->links() }}
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>     
    </div>    
  </div>
  

  <!--==========================
  Footer
============================-->
<footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="copyright">
            &copy; Copyright <strong>BPU-JMMI</strong>. All Rights Reserved
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Required JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/morphext/morphext.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/easing/easing.js"></script>

  <!-- Template Specisifc Custom Javascript File -->
  <script src="js/custom.js"></script>

  <script src="contactform/contactform.js"></script>


</body>

</html>
