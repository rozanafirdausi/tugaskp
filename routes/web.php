<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::post('/hlmadmin','PagesController@admin');
// Route::get('/login','PagesController@login');
Route::get('/','PagesController@homepage');
// Route::get('/user','PagesController@user');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/userbuku','BukuController@userbuku');

Route::get('/kategori','KategoriController@index');
Route::post('/kategori','KategoriController@store');
Route::post('/kategori/{id_kategori}/update', 'KategoriController@update');
Route::post('/kategori/{id_kategori}', 'KategoriController@delete');
Route::get('/trash','KategoriController@deleted');

Route::get('/buku','BukuController@index');
Route::post('/buku','BukuController@store');
Route::post('/buku/{id}/update', 'BukuController@update');
Route::post('/buku/{id}', 'BukuController@delete');
Route::get('/trashbuku','BukuController@deleted');

Route::get('/logout','Auth\LoginController@userLogout');

Route::group(['middleware' => ['web','auth']],function(){
	// Route::get('/','PagesController@homepage');
	
	
	Route::get('/user', function()
	{
		if(Auth::user()->admin == 0) 
		{
			return view ('user');
		}
		else
		{
			$users['users'] = \App\User::all();
			return view ('admin1',$users);
		}
	});
	
});
Route::get('/admin','AdminController@index');
// Route::prefix('admin')->group(function(){
// 	Route::get('/login','Auth\AdminLoginController@showLoginForm')->name('admin.login');
// 	Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
// 	Route::get('/','AdminController@index')->name('admin.dashboard'); //antara @index atau @admin hehe
// 	Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');
// });


Route::get('/kelola','KelolaController@index');
Route::post('/kelola','KelolaController@store');
Route::post('/kelola/{id_admin}/update', 'KelolaController@update');
Route::post('/kelola/{id_admin}', 'KelolaController@delete');
Route::get('/trashadmin', 'KelolaController@deleted');

Auth::routes();
