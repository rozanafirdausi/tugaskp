<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('notelp');
            $table->string('alamat');
            $table->string('password');
            $table->enum('jenis_kelamin',['L','P']);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('admins', function ($table) {
        $table->softDeletes();
        });
        // Schema::rename($admin, $admins);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('admins');
    }
}
