<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->integer('id_kategori')->unsigned();
            $table->string('penulis');
            $table->string('tahun');
            $table->string('penerbit');
            $table->timestamps();
        });

        Schema::table('buku', function (Blueprint $table) {
            $table->foreign('id_kategori')
            ->references('id_kategori')
            ->on('kategori')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
        
        Schema::table('buku', function ($table) {
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
